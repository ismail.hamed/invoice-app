import { defineAsyncComponent } from "vue";
import { createRouter, createWebHistory } from "vue-router";

import InvoicesList from "./pages/invoices/InvoicesList.vue";
import NotFound from "./pages/NotFound.vue";

const InvoiceDetail = defineAsyncComponent(() =>
  import("./pages/invoices/InvoiceDetail.vue")
);

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", redirect: "/invoices" },
    { path: "/invoices", component: InvoicesList },
    {
      path: "/invoices/:id",
      component: InvoiceDetail,
      props: true,
    },
    { path: "/:notFound(.*)", component: NotFound },
  ],
});

export default router;
