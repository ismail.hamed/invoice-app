export default {
  createInvoice(state, payload) {
    state.originInvoices.push(payload);
  },
  editInvoice(state, payload) {
    const index = state.originInvoices.findIndex(
      (invoice) => invoice.id === payload.id
    );
    state.originInvoices[index] = payload;
  },
  deleteInvoice(state, payload) {
    const index = state.originInvoices.findIndex(
      (invoice) => invoice.id === payload
    );
    state.originInvoices.splice(index, 1);
  },
  setInvoices(state, payload) {
    state.invoices = payload;
  },
  setInvoice(state, payload) {
    state.invoice = payload;
  },
  setStatus(state, payload) {
    state.invoice.status = payload;
  },
};
