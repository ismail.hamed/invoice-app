export default {
  async createInvoice(context, data) {
    // const userId = context.rootGetters.userId;
    // const coachData = {
    //   firstName: data.first,
    //   lastName: data.last,
    //   description: data.desc,
    //   hourlyRate: data.rate,
    //   areas: data.areas,
    // };

    // const token = context.rootGetters.token;

    // const response = await fetch(
    //   `https://vue-http-demo-85e9e.firebaseio.com/coaches/${userId}.json?auth=` +
    //     token,
    //   {
    //     method: "PUT",
    //     body: JSON.stringify(coachData),
    //   }
    // );

    // const responseData = await response.json();

    // if (!response.ok) {
    //   // error ...
    // }
    context.commit("createInvoice", data);
  },
  async deleteInvoice(context, id) {
    context.commit("deleteInvoice", id);
  },
  async editInvoice(context, data) {
    context.commit("editInvoice", data);
    context.commit("setInvoice", data);
  },
  async loadInvoices(context, payload) {
    var invoices = context.getters.originInvoices;
    const filters = payload?.filters;
    if (filters) {
      invoices = invoices.filter((invoice) => {
        if (filters.pending && invoice.status == "pending") {
          return true;
        }
        if (filters.draft && invoice.status == "draft") {
          return true;
        }
        if (filters.paid && invoice.status == "paid") {
          return true;
        }
        if (!filters.pending && !filters.draft && !filters.paid) {
          return true;
        }
      });
    }
    context.commit("setInvoices", invoices);
  },
  async loadInvoice(context, id) {
    const invoice = context.getters.originInvoices.find(
      (invoice) => invoice.id === id
    );
    context.commit("setInvoice", invoice);
  },
  async changeStatus(context, status) {
    context.commit("setStatus", status);
  },
};
