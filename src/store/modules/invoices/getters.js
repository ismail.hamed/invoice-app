export default {
  originInvoices(state) {
    return state.originInvoices;
  },
  invoices(state) {
    return state.invoices;
  },
  invoice(state) {
    return state.invoice;
  },
  hasInvoices(state) {
    return state.invoices && state.invoices.length > 0;
  },
};
