import { createStore } from "vuex";

import invoicesModule from "./modules/invoices/index.js";

const store = createStore({
  modules: {
    invoices: invoicesModule,
  },
});

export default store;
