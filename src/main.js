import { createApp, defineAsyncComponent } from "vue";
import titleMixin from "./mixins/titleMixin";

import router from "./router.js";
import store from "./store/index.js";
import App from "./App.vue";
import BaseCard from "./components/ui/BaseCard.vue";
import BaseButton from "./components/ui/BaseButton.vue";
import BaseSpinner from "./components/ui/BaseSpinner.vue";
import BaseStatus from "./components/ui/BaseStatus.vue";
import BaseInput from "./components/ui/BaseInput.vue";

import { clickOutside } from "./directive/clickOutside";
const BaseDialog = defineAsyncComponent(() =>
  import("./components/ui/BaseDialog.vue")
);

const app = createApp(App);

app.use(router);
app.use(store);

app.component("base-card", BaseCard);
app.component("base-button", BaseButton);
app.component("base-spinner", BaseSpinner);
app.component("base-dialog", BaseDialog);
app.component("base-status", BaseStatus);
app.component("base-input", BaseInput);

app.directive("click-outside", clickOutside);
app.mixin(titleMixin);

app.mount("#app");
